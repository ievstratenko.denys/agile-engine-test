import com.agileengine.Finder;
import org.junit.Assert;
import org.junit.Test;

public class Tests {

	private static final String ORIGIN_FILE = "html/origin.html";
	private static final String TARGET_ELEMENT_ID = "make-everything-ok-button";


	@Test
	public void testSample1EvilGemini() {
		simpleTest("html/sample-1-evil-gemini.html", "html > body > div > div > div[2] > div[0] > div > div[1] > a[1]");
	}


	@Test
	public void testSample2ContainerAndClone() {
		simpleTest("html/sample-2-container-and-clone.html", "html > body > div > div > div[2] > div[0] > div > div[1] > div > a");
	}


	@Test
	public void testSample3TheEscape() {
		simpleTest("html/sample-3-the-escape.html", "html > body > div > div > div[2] > div[0] > div > div[2] > a");
	}


	@Test
	public void testSample4TheMash() {
		simpleTest("html/sample-4-the-mash.html", "html > body > div > div > div[2] > div[0] > div > div[2] > a");
	}

	private void simpleTest(String otherSamplePath, String expectedPath) {
		Finder finder = new Finder(ORIGIN_FILE, TARGET_ELEMENT_ID, otherSamplePath);
		Assert.assertEquals(finder.findSimilarElement(), expectedPath);
	}
}
