package com.agileengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class Application {

	private static Logger LOGGER = LoggerFactory.getLogger(Application.class);
	private static final String DEFAULT_TARGET_ELEMENT_ID = "make-everything-ok-button";

	public static void main(String[] args) {

		LOGGER.info("Input args: [{}]", Arrays.toString(args));

		String targetElementId = args.length > 2 ? args[2] : DEFAULT_TARGET_ELEMENT_ID;
		LOGGER.info("Target element id={}", targetElementId);

		Finder finder = new Finder(args[0], targetElementId, args[1]);
		finder.findSimilarElement();
	}
}
