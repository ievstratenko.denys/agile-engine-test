package com.agileengine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Finder {

	private static Logger LOGGER = LoggerFactory.getLogger(Application.class);

	private static String CHARSET_NAME = "utf8";

	private static final List<String> SPECIAL_ATTRIBUTES = Arrays.asList("id", "class");

	private File originFile;
	private String targetElementId;
	private File otherSampleFile;

	public Finder(String originPath, String targetElementId, String otherSamplePath) {
		originFile = new File(originPath);
		this.targetElementId = targetElementId;
		otherSampleFile = new File(otherSamplePath);
	}

	public String findSimilarElement() {
		Element button = findElementInOrigin();

		if (button == null) {
			LOGGER.error("No element with id={} in the file {}", targetElementId, originFile.getAbsolutePath());
			return null;
		}

		Map<String, String> attributes = button.attributes()
				.asList().stream()
				.collect(Collectors.toMap(Attribute::getKey, Attribute::getValue));
		LOGGER.info("Target element attrs: [{}]", attributes);

		Element element = getElement(button, attributes);
		if (element == null) {
			LOGGER.error("No similar element found in {}", otherSampleFile.getAbsolutePath());
		}

		String path = getPath(element);

		LOGGER.info("RESULT: {}", path);
		return path;
	}

	private String getPath(Element element) {
		String path = "";
		for (Element e = element; e != null && !e.tagName().equals("#root"); e = e.parent()) {
			if (!path.isEmpty()) {
				path = " > " + path;
			}
			String tagName = e.tagName();
			List<Element> sameTagSiblings = e.parent().children().stream()
					.filter(s -> s.tagName().equals(tagName))
					.collect(Collectors.toList());
			int index = sameTagSiblings.size() > 1 ? sameTagSiblings.indexOf(e) : -1;

			path = tagName + (index >= 0 ? "[" + index + "]" : "") + path;
		}
		return path;
	}

	private Element getElement(Element originalElement, Map<String, String> attributes) {
		LOGGER.info("Parsing {}", otherSampleFile.getAbsolutePath());

		Collection<String> cssQueries = getCssBasicQueries(originalElement, attributes);
		LOGGER.info("CSS Basic Queries: {}", cssQueries);
		try {
			Document doc = Jsoup.parse(
					otherSampleFile,
					CHARSET_NAME,
					otherSampleFile.getAbsolutePath());
			return findBestCandidate(doc, cssQueries);
		} catch (IOException e) {
			LOGGER.error("Error reading [{}] file", otherSampleFile.getAbsolutePath(), e);
			return null;
		}
	}

	private Element findBestCandidate(Document doc, Collection<String> cssQueries) {

		Map<Element, Long> result = cssQueries.stream().map(doc::select).flatMap(Elements::stream)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

		Element bestCandidate = null;
		long maxCount = 0;
		for (Map.Entry<Element, Long> e : result.entrySet()) {
			if (e.getValue() > maxCount) {
				bestCandidate = e.getKey();
				maxCount = e.getValue();
			}
		}

		LOGGER.info("Among {} candidates found one with {} matching basic css queries", result.size(), maxCount);
		return bestCandidate;
	}

	private Collection<String> getCssBasicQueries(Element originalElement, Map<String, String> attributes) {
		Collection<String> cssQueries = new ArrayList<>();

		String id = originalElement.id();
		if (!id.isEmpty()) {
			cssQueries.add("#" + id);
		}

		Collection<String> classes = originalElement.classNames();
		cssQueries.addAll(classes.stream()
				.map(cls -> "." + cls)
				.collect(Collectors.toList()));

		cssQueries.addAll(
				attributes.keySet().stream()
						.filter(attrName -> !SPECIAL_ATTRIBUTES.contains(attrName))
						.map(attrName -> "[" + attrName + "]")
						.collect(Collectors.toList()));
		return cssQueries;
	}


	private Element findElementInOrigin() {
		try {
			Document doc = Jsoup.parse(
					originFile,
					CHARSET_NAME,
					originFile.getAbsolutePath());

			return doc.getElementById(targetElementId);
		} catch (IOException e) {
			LOGGER.error("Error reading [{}] file", originFile.getAbsolutePath(), e);
			return null;
		}
	}
}
